use std::collections::HashSet;
use std::sync::Arc;

use vulkano_win::VkSurfaceBuild;
use winit::{
    dpi::LogicalSize,
    event::{Event, WindowEvent},
    event_loop::{ControlFlow, EventLoop},
    window::{Window, WindowBuilder},
};

use vulkano::{
    buffer::{immutable::ImmutableBuffer, BufferAccess, BufferUsage, TypedBufferAccess},
    command_buffer::{AutoCommandBuffer, AutoCommandBufferBuilder, DynamicState},
    device::{Device, DeviceExtensions, Features, Queue},
    format::Format,
    framebuffer::{Framebuffer, FramebufferAbstract, RenderPassAbstract, Subpass},
    image::{swapchain::SwapchainImage, ImageUsage},
    instance::{
        debug::{DebugCallback, MessageSeverity, MessageType},
        layers_list, ApplicationInfo, Instance, InstanceExtensions, PhysicalDevice, QueueFamily,
        Version,
    },
    pipeline::{viewport::Viewport, GraphicsPipeline, GraphicsPipelineAbstract},
    swapchain::{
        acquire_next_image, AcquireError, Capabilities, ColorSpace, CompositeAlpha,
        FullscreenExclusive, PresentMode, SupportedPresentModes, Surface, Swapchain,
    },
    sync::{GpuFuture, SharingMode},
};

type ConcreteRenderPass = dyn RenderPassAbstract + Send + Sync;
type ConcreteFramebuffer = dyn FramebufferAbstract + Send + Sync;
type ConcreteGraphicsPipeline = dyn GraphicsPipelineAbstract + Send + Sync;

const WIDTH: u32 = 800;
const HEIGHT: u32 = 600;

const VALIDATION_LAYERS: &[&str] = &["VK_LAYER_LUNARG_standard_validation"];

#[cfg(all(debug_assertions))]
const ENABLE_VALIDATION_LAYERS: bool = true;
#[cfg(not(debug_assertions))]
const ENABLE_VALIDATION_LAYERS: bool = false;

fn device_extensions() -> DeviceExtensions {
    DeviceExtensions {
        khr_swapchain: true,
        ..DeviceExtensions::none()
    }
}

struct QueueFamilyIndices {
    graphics_family: i32,
    present_family: i32,
}

impl QueueFamilyIndices {
    fn new() -> Self {
        Self {
            graphics_family: -1,
            present_family: -1,
        }
    }

    fn is_complete(&self) -> bool {
        self.graphics_family >= 0 && self.present_family >= 0
    }
}

#[derive(Copy, Clone, Debug, Default)]
struct Vertex {
    pos: [f32; 2],
    color: [f32; 3],
}

impl Vertex {
    fn new(pos: [f32; 2], color: [f32; 3]) -> Self {
        Self { pos, color }
    }
}
vulkano::impl_vertex!(Vertex, pos, color);

#[allow(non_snake_case)]
fn VERTICES() -> [Vertex; 4] {
    [
        Vertex::new([-0.5, -0.5], [1.0, 0.0, 0.0]),
        Vertex::new([0.5, -0.5], [0.0, 1.0, 0.0]),
        Vertex::new([0.5, 0.5], [0.0, 0.0, 1.0]),
        Vertex::new([-0.5, 0.5], [0.0, 1.0, 0.0]),
    ]
}

const INDICES: [u16; 6] = [0, 1, 2, 2, 3, 0];

#[allow(unused)]
struct HelloTriangleApplication {
    instance: Arc<Instance>,
    debug_callback: Option<DebugCallback>,

    physical_device_index: usize,
    device: Arc<Device>,

    graphics_queue: Arc<Queue>,
    present_queue: Arc<Queue>,

    event_loop: EventLoop<()>,
    surface: Arc<Surface<Window>>,

    swap_chain: Arc<Swapchain<Window>>,
    swap_chain_images: Vec<Arc<SwapchainImage<Window>>>,
    swap_chain_framebuffers: Vec<Arc<ConcreteFramebuffer>>,

    render_pass: Arc<ConcreteRenderPass>,
    graphics_pipeline: Arc<ConcreteGraphicsPipeline>,

    vertex_buffer: Arc<dyn BufferAccess + Send + Sync>,
    index_buffer: Arc<dyn TypedBufferAccess<Content = [u16]> + Send + Sync>,
    command_buffers: Vec<Arc<AutoCommandBuffer>>,

    previous_frame_end: Option<Box<dyn GpuFuture>>,
    recreate_swap_chain: bool,
}

impl HelloTriangleApplication {
    pub fn initialize() -> Self {
        let instance = Self::create_instance();
        let debug_callback = Self::setup_debug_callback(&instance);
        let (event_loop, surface) = Self::init_window(&instance);
        let physical_device_index = Self::pick_physical_device(&instance, &surface);
        let (device, graphics_queue, present_queue) =
            Self::create_logical_device(&instance, &surface, physical_device_index);
        let (swap_chain, swap_chain_images) = Self::create_swap_chain(
            &instance,
            &surface,
            physical_device_index,
            &device,
            &graphics_queue,
            &present_queue,
        );
        let render_pass = Self::create_render_pass(&device, swap_chain.format());
        let graphics_pipeline =
            Self::create_graphics_pipeline(&device, swap_chain.dimensions(), &render_pass);
        let swap_chain_framebuffers = Self::create_framebuffers(&swap_chain_images, &render_pass);
        let vertex_buffer = Self::create_vertex_buffer(&graphics_queue);
        let index_buffer = Self::create_index_buffer(&graphics_queue);
        let command_buffers = Self::create_command_buffers(
            &device,
            graphics_queue.family(),
            &graphics_pipeline,
            &swap_chain_framebuffers,
            &vertex_buffer,
            &index_buffer,
        );
        let previous_frame_end = Some(Self::create_sync_objects(&device));

        Self {
            instance,
            debug_callback,

            physical_device_index,
            device,

            graphics_queue,
            present_queue,

            event_loop,
            surface,

            swap_chain,
            swap_chain_images,
            swap_chain_framebuffers,

            render_pass,
            graphics_pipeline,

            command_buffers,
            vertex_buffer,
            index_buffer,

            previous_frame_end,
            recreate_swap_chain: false,
        }
    }

    fn create_index_buffer(graphics_queue: &Arc<Queue>) -> Arc<dyn TypedBufferAccess<Content = [u16]> + Send + Sync> {
        Self::create_immutable_typed_buffer(
            graphics_queue,
            INDICES.iter().cloned(),
            BufferUsage::index_buffer(),
        )
    }

    fn create_vertex_buffer(graphics_queue: &Arc<Queue>) -> Arc<dyn BufferAccess + Send + Sync> {
        Self::create_immutable_buffer(
            graphics_queue,
            VERTICES().iter().cloned(),
            BufferUsage::vertex_buffer(),
        )
    }

    fn create_immutable_buffer<D, T>(
        graphics_queue: &Arc<Queue>,
        data: D,
        usage: BufferUsage,
    ) -> Arc<dyn BufferAccess + Send + Sync>
    where
        D: ExactSizeIterator<Item = T>,
        T: 'static + Sync + Send + Sized,
    {
        ImmutableBuffer::from_iter(data, usage, graphics_queue.clone())
            .expect("Failed to create immutable buffer")
            .0
    }

    fn create_immutable_typed_buffer<D, T>(
        graphics_queue: &Arc<Queue>,
        data: D,
        usage: BufferUsage,
    ) -> Arc<dyn TypedBufferAccess<Content = [T]> + Send + Sync>
    where
        D: ExactSizeIterator<Item = T>,
        T: 'static + Sync + Send + Sized,
    {
        ImmutableBuffer::from_iter(data, usage, graphics_queue.clone())
            .expect("Failed to create immutable buffer")
            .0
    }

    fn create_sync_objects(device: &Arc<Device>) -> Box<dyn GpuFuture> {
        Box::new(vulkano::sync::now(device.clone()))
    }

    fn create_command_buffers(
        device: &Arc<Device>,
        queue_family: QueueFamily,
        graphics_pipeline: &Arc<ConcreteGraphicsPipeline>,
        swap_chain_framebuffers: &Vec<Arc<ConcreteFramebuffer>>,
        vertex_buffer: &Arc<dyn BufferAccess + Send + Sync>,
        index_buffer: &Arc<dyn TypedBufferAccess<Content = [u16]> + Send + Sync>,
    ) -> Vec<Arc<AutoCommandBuffer>> {
        swap_chain_framebuffers
            .iter()
            .map(|fb| {
                Arc::new(
                    AutoCommandBufferBuilder::primary_simultaneous_use(
                        device.clone(),
                        queue_family,
                    )
                    .expect("Failed to start building command buffer")
                    .begin_render_pass(fb.clone(), false, vec![[0.0, 0.0, 0.0, 1.0].into()])
                    .expect("Failed to being render pass")
                    .draw_indexed(
                        graphics_pipeline.clone(),
                        &DynamicState::none(),
                        vec![vertex_buffer.clone()],
                        index_buffer.clone(),
                        (),
                        (),
                    )
                    .expect("Failed to draw to command buffer")
                    .end_render_pass()
                    .expect("Failed to end render pass")
                    .build()
                    .expect("Failed to build command buffer"),
                )
            })
            .collect()
    }

    fn create_framebuffers(
        swap_chain_images: &[Arc<SwapchainImage<Window>>],
        render_pass: &Arc<ConcreteRenderPass>,
    ) -> Vec<Arc<ConcreteFramebuffer>> {
        swap_chain_images
            .iter()
            .map(|image| {
                let fba: Arc<ConcreteFramebuffer> = Arc::new(
                    Framebuffer::start(render_pass.clone())
                        .add(image.clone())
                        .expect("Failed to add attachment to framebuffer")
                        .build()
                        .expect("Failed to create framebuffer"),
                );
                fba
            })
            .collect::<Vec<_>>()
    }

    fn create_render_pass(device: &Arc<Device>, color_format: Format) -> Arc<ConcreteRenderPass> {
        Arc::new(
            vulkano::single_pass_renderpass!(
                device.clone(),
                attachments: {
                  color: {
                    load: Clear,
                    store: Store,
                    format: color_format,
                    samples: 1,
                  }
                },
                pass: {
                  color: [color],
                  depth_stencil: {}
                }
            )
            .expect("Failed to create renderpass."),
        )
    }

    fn create_graphics_pipeline(
        device: &Arc<Device>,
        swap_chain_extent: [u32; 2],
        render_pass: &Arc<ConcreteRenderPass>,
    ) -> Arc<ConcreteGraphicsPipeline> {
        mod vert_shader {
            vulkano_shaders::shader! {
                ty: "vertex",
                path: "src/shader.vert",
            }
        }

        mod frag_shader {
            vulkano_shaders::shader! {
                ty: "fragment",
                path: "src/shader.frag",
            }
        }

        let vert_shader_mod = vert_shader::Shader::load(device.clone())
            .expect("failed to create vertex shader module!");
        let frag_shader_mod = frag_shader::Shader::load(device.clone())
            .expect("failed to create fragment shader module");

        let dimensions = [swap_chain_extent[0] as f32, swap_chain_extent[1] as f32];
        let viewport = Viewport {
            origin: [0.0, 0.0],
            dimensions,
            depth_range: 0.0..1.0,
        };

        Arc::new(
            GraphicsPipeline::start()
                .vertex_input_single_buffer::<Vertex>()
                .vertex_shader(vert_shader_mod.main_entry_point(), ())
                .triangle_list()
                .primitive_restart(false)
                .viewports(vec![viewport])
                .render_pass(
                    Subpass::from(render_pass.clone(), 0).expect("Failed to create subpass"),
                )
                .fragment_shader(frag_shader_mod.main_entry_point(), ())
                .depth_clamp(false) // default in vulkano
                .polygon_mode_fill() // default in vulkano
                .blend_pass_through() // default in vulkano
                .line_width(1.0)
                .cull_mode_back()
                .front_face_clockwise()
                .build(device.clone())
                .expect("Failed to create graphics pipeline"),
        )
    }

    fn init_window(instance: &Arc<Instance>) -> (EventLoop<()>, Arc<Surface<Window>>) {
        let event_loop = EventLoop::new();
        let window = WindowBuilder::new()
            .with_title("Vulkan")
            .with_inner_size(LogicalSize::new(f64::from(WIDTH), f64::from(HEIGHT)))
            .build_vk_surface(&event_loop, instance.clone())
            .unwrap();
        (event_loop, window)
    }

    fn create_instance() -> Arc<Instance> {
        let supported_extensions = InstanceExtensions::supported_by_core()
            .expect("failed to retrieve supported extensions");
        println!("Supported extensions: {:?}", supported_extensions);
        let required_extensions = Self::get_required_extensions();
        println!("Required extensions: {:?}", required_extensions);

        let app_info = ApplicationInfo {
            application_name: Some("Hello Triangle".into()),
            application_version: Some(Version {
                major: 1,
                minor: 0,
                patch: 0,
            }),
            engine_name: Some("No Engine".into()),
            engine_version: Some(Version {
                major: 1,
                minor: 0,
                patch: 0,
            }),
        };

        if ENABLE_VALIDATION_LAYERS && Self::check_validation_layer_support() {
            Instance::new(
                Some(&app_info),
                &required_extensions,
                VALIDATION_LAYERS.iter().cloned(),
            )
            .expect("failed to create Vulkan instance")
        } else {
            Instance::new(Some(&app_info), &required_extensions, None)
                .expect("failed to create Vulkan instance")
        }
    }

    fn check_validation_layer_support() -> bool {
        let layers: Vec<_> = layers_list()
            .unwrap()
            .map(|l| l.name().to_owned())
            .collect();
        VALIDATION_LAYERS
            .iter()
            .all(|lname| layers.contains(&lname.to_string()))
    }

    fn get_required_extensions() -> InstanceExtensions {
        let mut extensions = vulkano_win::required_extensions();
        if ENABLE_VALIDATION_LAYERS {
            extensions.ext_debug_utils = true;
        }

        extensions
    }

    fn setup_debug_callback(instance: &Arc<Instance>) -> Option<DebugCallback> {
        if !ENABLE_VALIDATION_LAYERS {
            return None;
        }

        let msg_type = MessageType {
            general: true,
            validation: true,
            performance: true,
        };
        let msg_svr = MessageSeverity {
            error: true,
            information: false,
            verbose: true,
            warning: true,
        };
        DebugCallback::new(&instance, msg_svr, msg_type, |msg| {
            println!("validation layer: {:?}", msg.description);
        })
        .ok()
    }

    fn create_logical_device(
        instance: &Arc<Instance>,
        surface: &Arc<Surface<Window>>,
        physical_device_index: usize,
    ) -> (Arc<Device>, Arc<Queue>, Arc<Queue>) {
        let physical_device = PhysicalDevice::from_index(&instance, physical_device_index).unwrap();
        let indices = Self::find_queue_families(&surface, &physical_device);

        let families = [indices.graphics_family, indices.present_family];
        use std::iter::FromIterator;
        let unique_queue_families: HashSet<&i32> = HashSet::from_iter(families.iter());

        let queue_priority = 1.0;
        let queue_families = unique_queue_families.iter().map(|i| {
            (
                physical_device.queue_families().nth(**i as usize).unwrap(),
                queue_priority,
            )
        });

        let (device, mut queues) = Device::new(
            physical_device,
            &Features::none(),
            &device_extensions(),
            queue_families,
        )
        .expect("failed to create logical device");

        let graphics_queue = queues.next().unwrap();
        let present_queue = queues.next().unwrap_or_else(|| graphics_queue.clone());

        (device, graphics_queue, present_queue)
    }

    fn pick_physical_device(instance: &Arc<Instance>, surface: &Arc<Surface<Window>>) -> usize {
        PhysicalDevice::enumerate(&instance)
            .position(|d| Self::is_device_suitable(surface, &d))
            .expect("failed to find a suitable GPU")
    }

    fn is_device_suitable(surface: &Arc<Surface<Window>>, device: &PhysicalDevice) -> bool {
        let indices = Self::find_queue_families(surface, device);
        let extensions_supported = Self::check_device_ext_support(device);

        let swap_chain_adequate = if extensions_supported {
            let capabilities = surface
                .capabilities(*device)
                .expect("failed to get surface capabilities");
            !capabilities.supported_formats.is_empty()
                && capabilities.present_modes.iter().next().is_some()
        } else {
            false
        };

        indices.is_complete() && extensions_supported && swap_chain_adequate
    }

    fn check_device_ext_support(device: &PhysicalDevice) -> bool {
        let available_extensions = DeviceExtensions::supported_by_device(*device);
        available_extensions.intersection(&device_extensions()) == device_extensions()
    }

    fn find_queue_families(
        surface: &Arc<Surface<Window>>,
        device: &PhysicalDevice,
    ) -> QueueFamilyIndices {
        let mut indices = QueueFamilyIndices::new();
        // TODO: replace index with id to simplify?
        for (i, queue_family) in device.queue_families().enumerate() {
            if queue_family.supports_graphics() {
                indices.graphics_family = i as i32;
            }

            if surface.is_supported(queue_family).unwrap() {
                indices.present_family = i as i32;
            }

            if indices.is_complete() {
                break;
            }
        }

        indices
    }

    fn choose_swap_surface_format(
        available_formats: &[(Format, ColorSpace)],
    ) -> (Format, ColorSpace) {
        // NOTE: the 'preferred format' mentioned in the tutorial doesn't seem to be
        // queryable in Vulkano (no VK_FORMAT_UNDEFINED enum)
        *available_formats
            .iter()
            .find(|(format, color_space)| {
                *format == Format::B8G8R8A8Unorm && *color_space == ColorSpace::SrgbNonLinear
            })
            .unwrap_or_else(|| &available_formats[0])
    }

    fn choose_swap_present_mode(available_present_modes: SupportedPresentModes) -> PresentMode {
        if available_present_modes.mailbox {
            PresentMode::Mailbox
        } else if available_present_modes.immediate {
            PresentMode::Immediate
        } else {
            PresentMode::Fifo
        }
    }

    fn choose_swap_extent(capabilities: &Capabilities) -> [u32; 2] {
        if let Some(current_extent) = capabilities.current_extent {
            current_extent
        } else {
            let mut actual_extent = [WIDTH, HEIGHT];
            actual_extent[0] = capabilities.min_image_extent[0]
                .max(capabilities.max_image_extent[0].min(actual_extent[0]));
            actual_extent[1] = capabilities.min_image_extent[1]
                .max(capabilities.max_image_extent[1].min(actual_extent[1]));
            actual_extent
        }
    }

    fn create_swap_chain(
        instance: &Arc<Instance>,
        surface: &Arc<Surface<Window>>,
        physical_device_index: usize,
        device: &Arc<Device>,
        graphics_queue: &Arc<Queue>,
        present_queue: &Arc<Queue>,
    ) -> (Arc<Swapchain<Window>>, Vec<Arc<SwapchainImage<Window>>>) {
        let physical_device = PhysicalDevice::from_index(&instance, physical_device_index).unwrap();
        let capabilities = surface
            .capabilities(physical_device)
            .expect("failed to get surface capabilities");

        let surface_format = Self::choose_swap_surface_format(&capabilities.supported_formats);
        let present_mode = Self::choose_swap_present_mode(capabilities.present_modes);
        let extent = Self::choose_swap_extent(&capabilities);

        let mut image_count = capabilities.min_image_count + 1;
        if capabilities.max_image_count.is_some()
            && image_count > capabilities.max_image_count.unwrap()
        {
            image_count = capabilities.max_image_count.unwrap();
        }

        let image_usage = ImageUsage {
            color_attachment: true,
            ..ImageUsage::none()
        };

        let indices = Self::find_queue_families(&surface, &physical_device);

        let sharing: SharingMode = if indices.graphics_family != indices.present_family {
            vec![graphics_queue, present_queue].as_slice().into()
        } else {
            graphics_queue.into()
        };

        let (swap_chain, images) = Swapchain::new(
            device.clone(),
            surface.clone(),
            image_count,
            surface_format.0,
            extent,
            1, // layers
            image_usage,
            sharing,
            capabilities.current_transform,
            CompositeAlpha::Opaque,
            present_mode,
            FullscreenExclusive::Allowed,
            true, // clipped
            surface_format.1,
        )
        .expect("failed to create swap chain!");

        (swap_chain, images)
    }

    fn recreate_swap_chain(
        instance: &Arc<Instance>,
        surface: &Arc<Surface<Window>>,
        physical_device_index: usize,
        device: &Arc<Device>,
        graphics_queue: &Arc<Queue>,
        present_queue: &Arc<Queue>,
    ) -> (
        Arc<Swapchain<Window>>,
        Vec<Arc<SwapchainImage<Window>>>,
        Vec<Arc<ConcreteFramebuffer>>,
        Arc<ConcreteRenderPass>,
        Arc<ConcreteGraphicsPipeline>,
        Vec<Arc<AutoCommandBuffer>>,
    ) {
        let (swap_chain, swap_chain_images) = Self::create_swap_chain(
            &instance,
            &surface,
            physical_device_index,
            &device,
            &graphics_queue,
            &present_queue,
        );
        let render_pass = Self::create_render_pass(&device, swap_chain.format());
        let graphics_pipeline =
            Self::create_graphics_pipeline(&device, swap_chain.dimensions(), &render_pass);
        let swap_chain_framebuffers = Self::create_framebuffers(&swap_chain_images, &render_pass);
        let vertex_buffer = Self::create_vertex_buffer(&graphics_queue);
        let index_buffer = Self::create_index_buffer(&graphics_queue);
        let command_buffers = Self::create_command_buffers(
            &device,
            graphics_queue.family(),
            &graphics_pipeline,
            &swap_chain_framebuffers,
            &vertex_buffer,
            &index_buffer,
        );
        (
            swap_chain,
            swap_chain_images,
            swap_chain_framebuffers,
            render_pass,
            graphics_pipeline,
            command_buffers,
        )
    }

    fn draw_frame(
        instance: &Arc<Instance>,
        surface: &Arc<Surface<Window>>,
        physical_device_index: usize,
        device: &Arc<Device>,
        swap_chain: &Arc<Swapchain<Window>>,
        command_buffers: &Vec<Arc<AutoCommandBuffer>>,
        graphics_queue: &Arc<Queue>,
        present_queue: &Arc<Queue>,
        recreate_swap_chain: bool,
    ) -> (
        bool,
        Option<Box<dyn GpuFuture>>,
        Option<(
            Arc<Swapchain<Window>>,
            Vec<Arc<SwapchainImage<Window>>>,
            Vec<Arc<ConcreteFramebuffer>>,
            Arc<ConcreteRenderPass>,
            Arc<ConcreteGraphicsPipeline>,
            Vec<Arc<AutoCommandBuffer>>,
        )>,
    ) {
        let mut recreated_swap_chain = None;
        if recreate_swap_chain {
            recreated_swap_chain = Some(Self::recreate_swap_chain(
                instance,
                surface,
                physical_device_index,
                device,
                graphics_queue,
                present_queue,
            ));
        }

        let (image_index, _, acquire_future) = match acquire_next_image(swap_chain.clone(), None) {
            Ok(r) => r,
            Err(AcquireError::OutOfDate) => {
                return (true, None, recreated_swap_chain);
            }
            Err(err) => panic!("{:?}", err),
        };

        match acquire_future
            .then_execute(graphics_queue.clone(), command_buffers[image_index].clone())
            .unwrap()
            .then_swapchain_present(present_queue.clone(), swap_chain.clone(), image_index)
            .then_signal_fence_and_flush()
        {
            Ok(future) => (false, Some(Box::new(future)), recreated_swap_chain),
            Err(vulkano::sync::FlushError::OutOfDate) => (
                true,
                Some(Box::new(vulkano::sync::now(device.clone()))),
                recreated_swap_chain,
            ),
            Err(e) => {
                eprintln!("{:?}", e);
                (
                    false,
                    Some(Box::new(vulkano::sync::now(device.clone()))),
                    recreated_swap_chain,
                )
            }
        }
    }

    #[allow(unused)]
    fn main_loop(self) -> ! {
        let event_loop = self.event_loop;
        let device = self.device;
        let graphics_queue = self.graphics_queue;
        let present_queue = self.present_queue;
        let physical_device_index = self.physical_device_index;
        let instance = self.instance;
        let surface = self.surface;

        let mut swap_chain = self.swap_chain;
        let mut swap_chain_images = self.swap_chain_images;
        let mut swap_chain_framebuffers = self.swap_chain_framebuffers;
        let mut render_pass = self.render_pass;
        let mut graphics_pipeline = self.graphics_pipeline;
        let mut command_buffers = self.command_buffers;
        let mut previous_frame_end = self.previous_frame_end;
        let mut recreate_swap_chain = self.recreate_swap_chain;

        let swindow_id = surface.window().id();
        event_loop.run(move |event, _, control_flow| {
            *control_flow = ControlFlow::Wait;

            match event {
                Event::WindowEvent {
                    event: WindowEvent::CloseRequested,
                    window_id,
                } if window_id == swindow_id => *control_flow = ControlFlow::Exit,
                _ => (),
            }
            let df = Self::draw_frame(
                &instance,
                &surface,
                physical_device_index,
                &device,
                &swap_chain,
                &command_buffers,
                &graphics_queue,
                &present_queue,
                recreate_swap_chain,
            );
            let (rsc, pfe, rswd) = df;
            recreate_swap_chain = rsc;
            previous_frame_end = pfe;
            if rswd.is_some() {
                let (sc, sci, scf, rp, gp, cb) = rswd.unwrap();
                swap_chain = sc;
                swap_chain_images = sci;
                swap_chain_framebuffers = scf;
                render_pass = rp;
                graphics_pipeline = gp;
                command_buffers = cb;
            }
        });
    }
}

fn main() {
    let app = HelloTriangleApplication::initialize();
    app.main_loop();
}
